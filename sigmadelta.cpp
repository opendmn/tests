#include <iostream>

#include <Eigen/Core>
#include <unsupported/Eigen/FFT>

#include <QtCore/QTime>


struct LowPassFilter {
    /* Set filter coefficients */
    double _b0 = 1.0, _b1 = 0.0, _b2 = 0.0;
    double _a1 = 0.0, _a2 = 0.0;

    /* Initialize history */
    double _x  = 0.0,  _y  = 0.0;
    double _x1 = 0.0, _y1 = 0.0;
    double _x2 = 0.0, _y2 = 0.0;

    LowPassFilter(double cutoffFreq, double samplingFreq, double slope = 1.0)
    {
        double gain = 1.0;

        qreal w0 = 2.0*M_PI * cutoffFreq / samplingFreq;

        qreal alpha = sin(w0) / (2.0 * sqrt( (gain + 1.0/gain)*(1/slope - 1) + 2));
//        qreal alpha = sin(w0) / (2.0 * 0.8);


        qreal x0 = (1.0 + alpha);
        qreal cos_w0 = cos(w0);

        _b0 = ((1.0 - cos_w0) / 2.0 ) / x0;
        _b1 = (1.0 - cos_w0) / x0;
        _b2 = ((1.0 - cos_w0) / 2.0 ) / x0;

//        _a0 = 1.0 + alpha;
        _a1 = (-2.0*cos_w0) / x0;
        _a2 = (1.0 - alpha) / x0;
    }

    void operator()(const Eigen::VectorXd & input, Eigen::VectorXd &output) {

        /* low pass filtering */
        for(int i = 0; i < input.size(); i++)
        {
            _y = _b0*input[i] + _b1*_x1 + _b2*_x2 - _a1*_y1 - _a2*_y2;
            _x2 = _x1;
            _x1 = input[i];
            _y2 = _y1;
            _y1 = _y;

            output[i] = _y;
        }

    }

};


int main(int argn, char **argv) {

    Eigen::VectorXd input(50);
    Eigen::VectorXd time(input.size());
    Eigen::VectorXd output(input.size());
    Eigen::VectorXd error1(input.size());
    Eigen::VectorXd error2(input.size());
    Eigen::VectorXd pdm(input.size());


    static double PDM_FREQ = 10e6;
    const double phase = M_PI;

    auto inputFilter = LowPassFilter(50e3, PDM_FREQ, 1.0);
    auto outputFilter = LowPassFilter(50e3, PDM_FREQ, 1.0);

    int step = 0;
    double I1 = 0.0, I2 = 0.0;
    Eigen::VectorXd outputFilterVector(input.size());

    /* specify how long  we want to sample  [s]*/
    const auto duration = 0.01;

    for(int blub = 0; blub < PDM_FREQ*duration/input.size(); blub++) {

        for(int i = 0; i < input.size(); i++) {
            time[i] = step/PDM_FREQ;
            input[i] = 0.95*sin(phase+2*M_PI/PDM_FREQ*0.5e3*(step++));
        }


        inputFilter(input, input);


        for(int i = 0; i < input.size(); i++) {
            error1[i] = I1;
            pdm[i] = (I1 <= I2)?1:-1;
            pdm[i] = (input[i] <= I1)?1:-1;
            I1 += (input[i]-pdm[i]);
            I2 += (I1-pdm[i]);
        }

        outputFilter(pdm, output);

        /* low pass filtering */
//        output = pdm;
        auto mean = output.mean();
        for(int i = 0; i < input.size(); i++) {
            outputFilterVector.topRows(outputFilterVector.size()-1) = outputFilterVector.bottomRows(outputFilterVector.size()-1);
            outputFilterVector[outputFilterVector.size()-1] = pdm[i];
            output[i] = outputFilterVector.mean();
            output[i] = mean;
//            output[i] = pdm[i];
        }


        for(int i = 0; i < pdm.size(); i++)
        {
            std::cout << time[i] << "\t" << input[i] << "\t" << output[i] << "\t" << error1[i] << "\t" << pdm[i] << std::endl;
        }


//        std::cout << "---" << std::endl;
//        std::cout << input.transpose() << std::endl;
//        std::cout << pdm.transpose() << std::endl;
//        std::cout << output.transpose() << std::endl;
    }
}
