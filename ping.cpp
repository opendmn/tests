#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>

#include <netpacket/packet.h>
#include <net/ethernet.h> /* the L2 protocols */

#include <arpa/inet.h>
#include <stdio.h>
#include <errno.h>
#include <net/if.h>
#include <string.h>
#include <unistd.h>



#include <iostream>


#define ETHER_TYPE 0x05dd
#define DEFAULT_IF "p1p1"
#define BUF_SIZ 1024



#define CHANNELS 16
#define FRAMELEN 32


static int NUM_SAMPLES = 16;

/* stores globablly the interface index */
int iface = 0;

double get_delta(struct timeval * start, struct timeval * end) {
	return (end->tv_sec+end->tv_usec/1.0e6)-(start->tv_sec+start->tv_usec/1.0e6);
}

inline double time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (tv.tv_sec+tv.tv_usec/1.0e6);
}





int write_data(int s) {

	char sendbuf[BUF_SIZ];
	struct ether_header *eh = (struct ether_header *) sendbuf;
	struct sockaddr_ll socket_address;
	
	int tx_len = 0;
	
	/* Construct the Ethernet header */
	memset(sendbuf, 0, BUF_SIZ);
	/* Ethernet header */
	eh->ether_shost[0] = 0x00;
	eh->ether_shost[1] = 0x24;
	eh->ether_shost[2] = 0xbb;
	eh->ether_shost[3] = 0x06;
	eh->ether_shost[4] = 0xbf;
	eh->ether_shost[5] = 0xae;
	
	/* 00:26:bb:06:bf:ae */
	eh->ether_dhost[0] = 0xFF;
	eh->ether_dhost[1] = 0xFF;
	eh->ether_dhost[2] = 0xFF;
	eh->ether_dhost[3] = 0xFF;
	eh->ether_dhost[4] = 0xFF;
	eh->ether_dhost[5] = 0xFF;
	
	/* Ethertype field */
	eh->ether_type = htons(ETHER_TYPE);
	tx_len += sizeof(struct ether_header);
	 
	
	/* Packet data, 32 samples a 32bit float */
    int plen = 64*4;
	for(;plen >= 0; plen--) {
		sendbuf[tx_len++] = 0x00;
	}
	
	/* Index of the network device */
	socket_address.sll_ifindex = iface;
	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address.sll_addr[0] = eh->ether_dhost[0];
	socket_address.sll_addr[1] = eh->ether_dhost[1];
	socket_address.sll_addr[2] = eh->ether_dhost[2];
	socket_address.sll_addr[3] = eh->ether_dhost[3];
	socket_address.sll_addr[4] = eh->ether_dhost[4];
	socket_address.sll_addr[5] = eh->ether_dhost[5];
	 
	/* Send packet */
	
	int n = sendto(s, sendbuf, tx_len, 0, 
	               (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll));
	if(n < 0) {
		printf("Send failed\n");
	}
	//printf("Send Bytes: %i\n", n);
	return n;
}


int read_data(int s) {
	
	uint8_t buf[BUF_SIZ];	
	struct ether_header *eh = (struct ether_header *) buf;

	int numbytes = 0, proto = 0;
	while(true) {
		
		while((numbytes = recvfrom(s, buf, BUF_SIZ, MSG_DONTWAIT, NULL, NULL)) == -1);
		//numbytes = recvfrom(s, buf, BUF_SIZ, 0, NULL, NULL);
		proto = ntohs(eh->ether_type);
		
		if(proto != ETHER_TYPE)
			continue;
		
		break;

	}
	
	//std::cout << "received packet size: " << std::dec << numbytes << ", type: 0x" << std::hex <<  ntohs(eh->ether_type) << std::endl;	
	return numbytes;
}

int main(int argc, char * argv[]) {

	
	/* create a raw socket */
	int s = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	
	if(s <= 0) {
		std::cout << "Error opening socket" << std::endl;
		return -1;
	}

	/* store socket number in global variable */
		
	/* get interface name */
	char ifName[IFNAMSIZ];
	if (argc > 1)
		strcpy(ifName, argv[1]);
	else
		strcpy(ifName, DEFAULT_IF);
		
	
	/* get the interface id */
	struct ifreq if_idx;
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(s, SIOCGIFINDEX, &if_idx) < 0) {
		perror("SIOCGIFINDEX");
	}
	iface = if_idx.ifr_ifindex;
	
	/* request multicast inputs to be delivered */
	struct packet_mreq mreq;
	memset(&mreq, 0x0, sizeof(mreq));
	mreq.mr_ifindex = if_idx.ifr_ifindex;
	mreq.mr_type |= PACKET_MR_ALLMULTI;
	setsockopt(s, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
	
	bool send_first = (argc > 2);

	struct timeval time_start, time_end;
	
	if(send_first) {

		for(int i = 0; i < 100; i++) {
			std::cout << "Sending first..." << std::endl;
			
			
			for(int i = 0; i < NUM_SAMPLES; i++) {
				
				/* write data */
				write_data(s);
				
				/* do stuff */
				//do_stuff(0);
				
				/* and wait for it */
				
				//diffs[i] = get_delta(&time_start, &time_end);
			}
			
			gettimeofday(&time_start, NULL);
			/* wait for response */
			read_data(s);
			
			/* measure time */
			gettimeofday(&time_end, NULL);
			
			
			/* print out the diffs  and take a mean */
			std::cout << "diff: " << get_delta(&time_start, &time_end) << std::endl;
			
	//		std::cout << "diff: ";
	//		double mean = 0.0;
	//		for(int i = 0; i < NUM_SAMPLES; i++) {
	//			std::cout << diffs[i] << "," ;
	//			mean += diffs[i];
	//		}
	//		mean /= double(NUM_SAMPLES);
	//		std::cout << " mean: " << mean << std::endl;
		}
		
	} else {
		while(true) {	
			double start = 0.0, end = 0.0;
	
			for(int i = 0; i < NUM_SAMPLES; i++) {
				read_data(s);
				
				if(i == 0) {
					start = time();
				}
				
			}
			
			write_data(s);
			
			end = time();
		
            std::cout << "diff: " << (end-start)/NUM_SAMPLES << std::endl;
		}
	}
	
	close(s);
}
