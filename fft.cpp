#include <iostream>

#include <Eigen/Core>
#include <unsupported/Eigen/FFT>

#include <QtCore/QTime>




int main(int argc, char *argv[])
{


    Eigen::FFT<float> fft;


    Eigen::VectorXf input(64);

    Eigen::VectorXf output(input.size());
    output.setZero();
    Eigen::VectorXf oadd(input.size());
    oadd.setZero();




    /* create the filter */
    Eigen::VectorXcf filter(input.size()*2);
    {

        filter.setZero();
        filter.topRows(filter.size()/2-10).setConstant(1.0);
    }

    int step = 0;

    Eigen::VectorXf timevec(input.size()*2);
    Eigen::VectorXcf freqvec(timevec.size());

    QTime t;
    t.start();

    for(int j = 0; j < 1024; j++) {



        /* create input signal here */
        {
//            input.middleRows(0, 32).setConstant(1.0);

            for(int i = 0; i < input.size(); i++) {
                input[i] = sin(2*M_PI/48000.0*(step++)*50);
            }

        }



        timevec.topRows(input.size()) = input;
        timevec.bottomRows(input.size()).setConstant(0.0);


        fft.fwd(freqvec,timevec);

//        for(int i = 0; i  < freqvec.size(); i++) {
//            std::cout << i << "\t" << timevec[i] << "\t" << filter[i] << "\t" << freqvec[i] << std::endl;
//        }

        freqvec.array() *= filter.array();

        fft.inv(timevec, freqvec);
        output = timevec.topRows(output.size());
        output += oadd;

        oadd = timevec.bottomRows(output.size());

//        for(int i = 0; i  < output.size(); i++) {
//            std::cout << i << "\t" <<  output[i] << std::endl;
//        }

    }

    std::cout << "ifft:" << t.elapsed()/1000.0/1024.0 << std::endl;

    return 0;
}
