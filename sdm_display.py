#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy
import pylab

signals_full = numpy.genfromtxt('bla.txt', delimiter='\t')
signals = signals_full[0:20000,:]
time = signals[:,0]

input = signals[:,1]
input_full = signals_full[:,1]

output = signals[:,2]
output_full = signals_full[:,2]

error = signals[:,3]

pdm = signals[:,4]
pdm_full = signals_full[:,4]

print(signals.shape)

pylab.figure()

# first plot pdm
pylab.plot(time, pdm, alpha=0.25, marker="o", linestyle=None)
#pylab.stem(time, pdm)
dt = time[1]-time[0]
#pylab.bar(time-dt/2, pdm, dt, color="C9")

# then overdraw it with the analog signals
for label, array in [("output", output), ("input", input)]: # ("error", error)
    pylab.plot(time, array, label=label)

pylab.legend()

pylab.figure()
pylab.magnitude_spectrum(pdm_full, Fs=10e6, label="pdm",  scale='dB')
pylab.magnitude_spectrum(output_full, Fs=10e6, label="output",  scale='dB')
pylab.magnitude_spectrum(input_full, Fs=10e6, label="input",  scale='dB')
pylab.legend()



pylab.show()



