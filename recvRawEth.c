/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 */



#include <sys/time.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <errno.h>

#define DEST_MAC0	0xFF
#define DEST_MAC1	0xFF
#define DEST_MAC2	0xFF
#define DEST_MAC3	0xFF
#define DEST_MAC4	0xFF
#define DEST_MAC5	0xFF

#define ETHER_TYPE	0x05dd

#define DEFAULT_IF	"eth0"
#define BUF_SIZ		1024


int iface = 0;

double get_delta(struct timeval * start, struct timeval * end) {
	return (end->tv_sec/1000.0+end->tv_usec/1000.0)-(start->tv_sec/1000.0+start->tv_usec/1000.0);
}

int write_data(int s, char * frame) {

	char sendbuf[BUF_SIZ];
	
	struct ether_header *eh = (struct ether_header *) sendbuf;
	struct sockaddr_ll socket_address;
	
	int tx_len = 0;
	
	/* Construct the Ethernet header */
	memset(sendbuf, 0, BUF_SIZ);
	/* Ethernet header */
	eh->ether_shost[0] = 0x00;
	eh->ether_shost[1] = 0x24;
	eh->ether_shost[2] = 0xbb;
	eh->ether_shost[3] = 0x06;
	eh->ether_shost[4] = 0xbf;
	eh->ether_shost[5] = 0xae;
	
	/* 00:26:bb:06:bf:ae */
	eh->ether_dhost[0] = DEST_MAC0;
	eh->ether_dhost[1] = DEST_MAC1;
	eh->ether_dhost[2] = DEST_MAC2;
	eh->ether_dhost[3] = DEST_MAC3;
	eh->ether_dhost[4] = DEST_MAC4;
	eh->ether_dhost[5] = DEST_MAC5;
	/* Ethertype field */
	eh->ether_type = htons(ETHER_TYPE);
	tx_len += sizeof(struct ether_header);
	 
	
	/* Packet data, 32 samples a 32bit float */
	int plen = 32*4;
	for(;plen >= 0; plen--) {
		sendbuf[tx_len++] = 0x00;
	}
	
	/* Index of the network device */
	socket_address.sll_ifindex = iface;
	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address.sll_addr[0] = eh->ether_dhost[0];
	socket_address.sll_addr[1] = eh->ether_dhost[1];
	socket_address.sll_addr[2] = eh->ether_dhost[2];
	socket_address.sll_addr[3] = eh->ether_dhost[3];
	socket_address.sll_addr[4] = eh->ether_dhost[4];
	socket_address.sll_addr[5] = eh->ether_dhost[5];
	 
	/* Send packet */
	
	int n = sendto(s, sendbuf, tx_len, 0, 
	               (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll));
	if(n < 0) {
		printf("Send failed\n");
	}
	printf("Send Bytes: %i\n", n);
	return n;
}


int read_data(int s) {
	
	uint8_t buf[BUF_SIZ];
	
	printf("listener: Waiting to recvfrom...\n");
	int numbytes = 0;
	//while((numbytes = recvfrom(s, buf, BUF_SIZ, MSG_DONTWAIT, NULL, NULL)) == -1);
	
	numbytes = recvfrom(s, buf, BUF_SIZ, 0, NULL, NULL);
	
	printf("listener: got packet %i bytes\n", numbytes);
	return numbytes;
}


int main(int argc, char *argv[])
{
	char sender[INET6_ADDRSTRLEN];
	int sockfd, ret, i;
	int sockopt;
	ssize_t numbytes;
	
	struct ifreq ifopts;	/* set promiscuous mode */
	struct ifreq if_ip;	/* get ip addr */
	struct ifreq if_idx; /* get interface id */
	
	struct sockaddr_storage their_addr;
	uint8_t buf[BUF_SIZ];
	char ifName[IFNAMSIZ];
	
	/* Get interface name */
	if (argc > 1)
		strcpy(ifName, argv[1]);
	else
		strcpy(ifName, DEFAULT_IF);

	/* Header structures */
	struct ether_header *eh = (struct ether_header *) buf;
	struct iphdr *iph = (struct iphdr *) (buf + sizeof(struct ether_header));
	struct udphdr *udph = (struct udphdr *) (buf + sizeof(struct iphdr) + sizeof(struct ether_header));

	memset(&if_ip, 0, sizeof(struct ifreq));

	/* Open PF_PACKET socket, listening for EtherType ETHER_TYPE */
	if ((sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETHER_TYPE))) == -1) {
		perror("listener: socket");	
		return -1;
	}

	/* Set interface to promiscuous mode - do we need to do this every time? */
	strncpy(ifopts.ifr_name, ifName, IFNAMSIZ-1);
	ioctl(sockfd, SIOCGIFFLAGS, &ifopts);
	ifopts.ifr_flags |= IFF_PROMISC;
	ioctl(sockfd, SIOCSIFFLAGS, &ifopts);
		
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
		perror("SIOCGIFINDEX");
	
	iface = if_idx.ifr_ifindex;
	
	/* Allow the socket to be reused - incase connection is closed prematurely */
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof sockopt) == -1) {
		perror("setsockopt");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
	/* Bind to device */
	if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, ifName, IFNAMSIZ-1) == -1)	{
		perror("SO_BINDTODEVICE");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
	
	/* see if we have a argument. in this case we first send and then receive */
	int first_send = (argc > 2);
	
	
	int iloop = 0;
	
	/* capture amount of time needed for roundtime */
	struct timeval start_time, end_time;
	
	
	/* if we are triggering roundtrip, put some time stats out */
	if(first_send) {
		printf("first sending packet");	
		
		for(iloop = 0; iloop < 10; iloop++) {
			gettimeofday(&start_time, NULL);
			write_data(sockfd, buf);
			read_data(sockfd);	
			gettimeofday(&end_time, NULL);
			
			printf("diff: %f\n", get_delta(&start_time, &end_time));
		}
		
	} else {
	
		for(iloop = 0; iloop < 10; iloop++) {	
			read_data(sockfd);
			write_data(sockfd, buf);
		}
	}

	close(sockfd);
	return ret;
}